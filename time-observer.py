"""
A sample project to check how to use the RxPy library.

In this program, I'm using a timer trigger to notify
observers to do something.

TimerObserver
=============
An observable class is responsible of notifying its observers
each predetermined interval (in this sample, each 3 seconds).

PrintObserver
=============
An observer class that prints on console what it receives
of its observables.

Reference: samples at https://github.com/ReactiveX/RxPY
"""
from rx import Observable, Observer
import json


class PrintObserver(Observer):

    def on_next(self, value):
        print("Received {0}".format(value))

    def on_completed(self):
        print("Done!")

    def on_error(self, error):
        print(">> Error Occurred: {0}".format(error))


class TimerObserver():

    def initiate_timer(self):
        self.obs1 = Observable.interval(3000)\
            .map(lambda i: "{0} Mississippi".format(i))


timer = TimerObserver()
timer.initiate_timer()
timer.obs1.subscribe(PrintObserver())

input("Press <enter> key to quit\n")
