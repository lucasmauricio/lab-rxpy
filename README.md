# Python RX Lab

This is an experimental project to develop some reactive programs using the RxPy library (a client for ReactiveX).

By the way, I suggest to use [VirtualEnv](https://virtualenv.pypa.io/) for running it without changing your system configuration.
The file `requirements.txt` describes all projects' dependencies.

## Running the programs

Just a time observer: each 3 seconds, we print the message we receive:
```shell
python time-observer.py
```

Improving the time observer: each 3 seconds, we request data from a REST API [1]:
```shell
python time-observer-with-restclient.py
```

Use the time observer to notify that a worker was found: each 3 seconds, we request data from a REST API and notify the worker found [1]:
```shell
python time-observer-notifying.py
```


[1] In this case, we also need to run the REST API server:
```shell
python server.py
```

PS: a fixture to the worker API:
```shell
curl --request POST \
  --url http://localhost:5001/api/client/ \
  --header 'content-type: application/json' \
  --data '{
	"name":"John Doe", "age": 20
}'
```
