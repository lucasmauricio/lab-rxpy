"""
A mock REST API to serve client data.
"""
from flask import Flask, request
import logging
import json


class PseudoDatabase():
    def __init__(self):
        self.data = None
    def get_client(self):
        return self.data
    def post_client(self, new_client):
        self.data = new_client
        return True
    def delete_client(self):
        if self.data:
            self.data = None
            return True
        else:
            return False


app = Flask(__name__)
db = PseudoDatabase()

@app.route('/api/client/', methods=['GET'])
def get_all_api():
    result = db.get_client()
    if not result:
        return "Not found.", 404
    return json.dumps(result), 200

@app.route('/api/client/', methods=['POST'])
def post_api():
    """Expects a JSON object which must contain 'name' and 'age'.
    For instance:
      {"name":"John Doe", "age": 20}
    """
    client_data = request.json
    if not client_data:
        return "Bad request: missing required data", 400
    result = db.post_client(client_data)
    if not result:
        return 'unmapped-error', 500
    else:
        return "", 201

@app.route('/api/client/', methods=['DELETE'])
def delete_api():
    result = db.delete_client()
    if not result:
        return "Not found.", 404
    else:
        return "", 204


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s,%(msecs)-3d - %(name)-12s - %(levelname)-8s => %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.DEBUG)

    app.run('', 5001, threaded=True, debug=True)
