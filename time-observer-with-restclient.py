"""
A sample project to check how to use the RxPy library.

In this program, I'm extending the "time-observer.py" program
to consume a REST API (the base of the program base is still
a timer trigger).

TimerObserver
=============
An observable class is responsible of notifying its observers
each predetermined interval (in this sample, each 3 seconds).

PrintObserver
=============
An observer class that prints on console what it receives
of its observables.

RestClientObserver
==================
An observer class that gets data from a REST service and
prints it on console.

PS: this program needs "rest-api-server.py" to work properly.

Reference: samples at https://github.com/ReactiveX/RxPY
"""
from rx import Observable, Observer
import requests
import json


class PrintObserver(Observer):

    def on_next(self, value):
        print("Received {0}".format(value))

    def on_completed(self):
        print("Done!")

    def on_error(self, error):
        print("Error Occurred: {0}".format(error))


class RestClientObserver(Observer):
    """Get data from a REST service."""

    def on_next(self, value):
        url = "http://localhost:5001/api/client/"
        try:
            r = requests.get(url, headers={"Content-Type": "application/json"})
            r.raise_for_status()
            data = json.loads(r.text)
            print(f"Data gathered from service: {data}")
        except requests.exceptions.HTTPError as e:
            print(f"Error requesting data from service: {e}")
        except Exception as e:
            print(f"Error requesting data from service: {e}")

    def on_completed(self):
        print("REST client observer is completed!")

    def on_error(self, error):
        print("REST client observer got an error: {0}".format(error))


class TimerObserver():

    def initiate_timer(self):
        self.obs1 = Observable.interval(3000)\
            .map(lambda i: "{0} Mississippi".format(i))


timer = TimerObserver()
timer.initiate_timer()
timer.obs1.subscribe(PrintObserver())
timer.obs1.subscribe(RestClientObserver())


input("Press <enter> key to quit\n")
