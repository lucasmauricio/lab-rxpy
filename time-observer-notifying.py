"""
A sample project to check how to use the RxPy library.

In this program, I'm extending the "time-observer-with-restclient.py"
program to notify another kind of observers (note that they are not
time-dependent).

TimerObserver
=============
Now it is a subject class. A subject class is an observable and an
observer class.
Likewise the other programs in this project, as observable, it is
responsible of notifying its observers each predetermined interval
(in this sample, each 3 seconds).
It also transmits an object to its observers in a second observable
(an observable not time-dependent).

PrintObserver
=============
An observer class that prints on console what it receives
of its observables.

RestClientObserver
==================
An observer class that gets data from a REST service and
prints it on console.

WorkerObserver
==============
An observer class that analyzes received data and says if the
worker may retire or should work longer.
This analysis is printed on console.

PS: this program needs "rest-api-server.py" to work properly.

Reference: samples at https://github.com/ReactiveX/RxPY




A sample project to check how to use the RxPy library.

In this program, I'm
A sample program to check how to use the RxPy library.


The TimerObserver class is in charge of orchestrate
program execution.




Use the time observer to notify that a worker was found: each 3 seconds, we request data from a REST API and notify the worker found [1]:


https://github.com/ReactiveX/RxPY
"""
from rx import Observable, Observer
from rx.subjects import Subject
import requests
import json


class PrintObserver(Observer):
    """Observer to print data."""

    def on_next(self, value):
        print("Received {0}".format(value))

    def on_completed(self):
        print("Done!")

    def on_error(self, error):
        print("Error Occurred: {0}".format(error))


class RestClientObserver(Observer):
    """Observer to get data from a REST service."""

    def on_next(self, value):
        url = "http://localhost:5001/api/client/"
        try:
            r = requests.get(url, headers={"Content-Type": "application/json"})
            r.raise_for_status()
            data = json.loads(r.text)
            print(f"Data gathered from service: {data}")
            # triggers the observers notification about this data
            timer.trigger_worker_observer(data)
        except requests.exceptions.HTTPError as e:
            print(f"Error requesting data from service: {e}")
        except Exception as e:
            print(f"Error requesting data from service: {e}")

    def on_completed(self):
        print("REST client observer is completed!")

    def on_error(self, error):
        print("REST client observer got an error: {0}".format(error))


class WorkerObserver(Observer):
    """Observer to verify if it is time to retire the worker."""

    def on_next(self, data):
        if 'age' not in data or 'name' not in data:
            print(f"Data in wrong pattern: '{data}'")
        elif data['age'] > 65:
            print(f"Congrats {data['name']}, it's time to rest! Enjoy it.")
        else:
            print(f"{data['name']}, we're counting on you.")

    def on_completed(self):
        print("Worker observer is completed!")

    def on_error(self, error):
        print("Worker observer catch an error: {0}".format(error))


class TimerObserver():

    def initiate_timer(self):
        self.obs1 = Observable.interval(3000)#\
        # .map(lambda i: "{0} Mississippi".format(i))
        self.obs1.subscribe(lambda s: print(s))

    def initiate_stream(self):
        self.obs2 = Subject()

    def trigger_worker_observer(self, worker):
        self.obs2.on_next(worker)


timer = TimerObserver()
timer.initiate_timer()
timer.obs1.subscribe(RestClientObserver())
timer.initiate_stream()
timer.obs2.subscribe(PrintObserver())
timer.obs2.subscribe(WorkerObserver())


input("Press any key to quit\n")
